The README would normally document whatever steps are necessary to make your application operational.*

Introduction:
The purpose of this project is to run a restaurant simulation using various libraries and functional programming functions

Part 1:
The first part is to give a short analysis of the restaurant data.
Usage of libraries such as pandas, matplotlib qnd numpy.
Display data visualization graphs.

Part 2:
This part involves clustering of data and identifying the k-means using k-means library.
The code will display labels and a 3D diagram

Part 3:
The objective of this part is to determine the distributions of clients.
The probability of each client ordering which course.
The distribution of dishes per course and the cost of the drinks per course.


Part 4:
This part involves creating a final simulation using random distribution.
Creation of object oriented programming.
Also involves creating a seperate file for 5 years and saving it seperately.
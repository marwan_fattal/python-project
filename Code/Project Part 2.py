def cluster(input_data):
    x = input_data[['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']].values
    # finding the clusters based on input matrix "x"
    model = KMeans(n_clusters=4, init="k-means++", max_iter=3500, n_init=10, random_state=0)
    y_clusters = model.fit_predict(x)
    sns.set_theme(style="darkgrid")
    s = sns.countplot(y_clusters)
    # 3d scatterplot using matplotlib
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111, projection='3d')
    # A scatter for cluster 1
    ax.scatter(x[y_clusters == 0, 0], x[y_clusters == 0, 1], x[y_clusters == 0, 2], s=10, color='blue',
               label="cluster 1")
    # A scatter for cluster 2
    ax.scatter(x[y_clusters == 1, 0], x[y_clusters == 1, 1], x[y_clusters == 1, 2], s=10, color='orange',
               label="cluster 2")
    # A scatter for cluster 3
    ax.scatter(x[y_clusters == 2, 0], x[y_clusters == 2, 1], x[y_clusters == 2, 2], s=10, color='green',
               label="cluster 3")
    # A scatter for cluster 4
    ax.scatter(x[y_clusters == 3, 0], x[y_clusters == 3, 1], x[y_clusters == 3, 2], s=10, color='red',
               label="cluster 4")
    # Set axis labels
    ax.set_xlabel('FIRST COURSE')
    ax.set_ylabel('SECOND COURSE')
    ax.set_zlabel('THIRD COURSE')
    ax.legend()
    plt.show()


if __name__ == '__main__':
    # the input file
    file = "part1.csv"
    # check if csv exist
    if os.path.isfile(file):
        # read the csv as a data frame
        data = pd.read_csv(file, sep=',')
        # Get the cost (sum and average) per course
        costs = cost_per_course(data)
        # Get the csv with the real costs columns
        add_real_costs(data)
        # Draw the costs plots
        draw(costs)
        # Draw the clusters
        cluster(data)
    else:
        print("File not found")
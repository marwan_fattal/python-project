import os
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame
from sklearn.cluster import KMeans
import seaborn as sns


starters = [{'name': "Soup", 'price': 3},
            {'name': "Tomato-Mozzarella", 'price': 15},
            {'name': "Oysters", 'price': 20}]

mains = [{'name': "Salad", 'price': 9},
         {'name': "Spaghetti", 'price': 20},
         {'name': "Steak", 'price': 25},
         {'name': "Lobster", 'price': 40}]

desserts = [{'name': "Ice cream", 'price': 15},
            {'name': "Pie", 'price': 10}]


def cost_per_course(data):
    number_of_clients = len(data.index)
    # Get the Sum per course
    sum_first_course = data["FIRST_COURSE"].sum()
    sum_second_course = data["SECOND_COURSE"].sum()
    sum_third_course = data["THIRD_COURSE"].sum()
    # Get the average per course
    av_first_course = round(sum_first_course / number_of_clients, 3)
    av_second_course = round(sum_second_course / number_of_clients, 3)
    av_third_course = round(sum_third_course / number_of_clients, 3)
    # Store the results in a list then return it
    result = [
        ['FIRST\nCOURSE', sum_first_course, av_first_course],
        ['SECOND\nCOURSE', sum_second_course, av_second_course],
        ['THIRD\nCOURSE', sum_third_course, av_third_course]
    ]
    return result


def draw(input_list):
    columns = list(zip(*input_list))
    fig, (ax_sum, ax_avg) = plt.subplots(1, 2)
    fig.suptitle('Costs per course')
    # the horizontal axis
    x = columns[0]
    # the vertical axis for the sum
    y_sum = columns[1]
    # the vertical axis for the average
    y_avg = columns[2]
    # The plot of the distribution of the cost in each course
    ax_sum.set_title("Sum per course")
    ax_sum.plot(x, y_sum)
    # The barplot of the cost per course
    ax_avg.set_title("Average cost per course")
    ax_avg.bar(x, y_avg)
    # Add the value of the average bars
    for i, j in zip(x, y_avg):
        ax_avg.annotate(str(j), xy=(i, j))
    # show the plots
    plt.show()


def add_real_costs(data_frame):
    output_file = "part1_output.csv"
    # Add new column FIRST_COURSE_PLATE and fill the values
    data_frame['FIRST_COURSE_PLATE'] = data_frame.apply(
        lambda x: price_details(starters, x['FIRST_COURSE'])['plat_name'], axis=1)
    # Add new column FIRST_COURSE_PRICE and fill the values
    data_frame['FIRST_COURSE_PRICE'] = data_frame.apply(
        lambda x: price_details(starters, x['FIRST_COURSE'])['plat_price'], axis=1)
    # Add new column FIRST_COURSE_DRINK_PRICE and fill the values
    data_frame['FIRST_COURSE_DRINK_PRICE'] = data_frame.apply(
        lambda x: price_details(starters, x['FIRST_COURSE'])['drink_price'], axis=1)
    # Add new column SECOND_COURSE_PLATE and fill the values
    data_frame['SECOND_COURSE_PLATE'] = data_frame.apply(
        lambda x: price_details(mains, x['SECOND_COURSE'])['plat_name'], axis=1)
    # Add new column SECOND_COURSE_PRICE and fill the values
    data_frame['SECOND_COURSE_PRICE'] = data_frame.apply(
        lambda x: price_details(mains, x['SECOND_COURSE'])['plat_price'], axis=1)
    # Add new column SECOND_COURSE_DRINK_PRICE and fill the values
    data_frame['SECOND_COURSE_DRINK_PRICE'] = data_frame.apply(
        lambda x: price_details(mains, x['SECOND_COURSE'])['drink_price'], axis=1)
    # Add new column THIRD_COURSE_PLATE and fill the values
    data_frame['THIRD_COURSE_PLATE'] = data_frame.apply(
        lambda x: price_details(desserts, x['THIRD_COURSE'])['plat_name'], axis=1)
    # Add new column THIRD_COURSE_PRICE and fill the values
    data_frame['THIRD_COURSE_PRICE'] = data_frame.apply(
        lambda x: price_details(desserts, x['THIRD_COURSE'])['plat_price'], axis=1)
    # Add new column THIRD_COURSE_DRINK_PRICE and fill the values
    data_frame['THIRD_COURSE_DRINK_PRICE'] = data_frame.apply(
        lambda x: price_details(desserts, x['THIRD_COURSE'])['drink_price'], axis=1)
    # Export the new data frame to a new csv file
    data_frame.to_csv(output_file, sep=',')


def price_details(course_list, paid):
    # If course value not 0, check the paid amount with each item from the list defined
    if paid > 0:
        for item in course_list:
            # compute the drink value and compare
            # if value is less or equal to 0, the drink costs 0
            # Drink value must be less then item price
            drink = paid - item["price"]
            if 0 <= drink < item["price"]:
                # return the results as dict
                return {"plat_name": item["name"], "plat_price": item["price"], "drink_price": drink}
    else:
        # Return for courses with 0 value
        return {"plat_name": "", "plat_price": 0.0, "drink_price": 0.0}

    def cluster(input_data):
        x = input_data[['FIRST_COURSE', 'SECOND_COURSE', 'THIRD_COURSE']].values
        # finding the clusters based on input matrix "x"
        model = KMeans(n_clusters=4, init="k-means++", max_iter=3500, n_init=10, random_state=0)
        y_clusters = model.fit_predict(x)
        sns.set_theme(style="darkgrid")
        s = sns.countplot(y_clusters)
        # 3d scatterplot using matplotlib
        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(111, projection='3d')
        # A scatter for cluster 1
        ax.scatter(x[y_clusters == 0, 0], x[y_clusters == 0, 1], x[y_clusters == 0, 2], s=10, color='blue',
                   label="cluster 1")
        # A scatter for cluster 2
        ax.scatter(x[y_clusters == 1, 0], x[y_clusters == 1, 1], x[y_clusters == 1, 2], s=10, color='orange',
                   label="cluster 2")
        # A scatter for cluster 3
        ax.scatter(x[y_clusters == 2, 0], x[y_clusters == 2, 1], x[y_clusters == 2, 2], s=10, color='green',
                   label="cluster 3")
        # A scatter for cluster 4
        ax.scatter(x[y_clusters == 3, 0], x[y_clusters == 3, 1], x[y_clusters == 3, 2], s=10, color='red',
                   label="cluster 4")
        # Set axis labels
        ax.set_xlabel('FIRST COURSE')
        ax.set_ylabel('SECOND COURSE')
        ax.set_zlabel('THIRD COURSE')
        ax.legend()
        plt.show()

    if __name__ == '__main__':
        # the input file
        file = "part1.csv"
        # check if csv exist
        if os.path.isfile(file):
            # read the csv as a data frame
            data = pd.read_csv(file, sep=',')
            # Get the cost (sum and average) per course
            costs = cost_per_course(data)
            # Get the csv with the real costs columns
            add_real_costs(data)
            # Draw the costs plots
            draw(costs)
            # Draw the clusters
            cluster(data)
        else:
            print("File not found")


